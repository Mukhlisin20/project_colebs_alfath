@extends('layouts.template')

     @section('content')
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="{{ route('register') }}" method="POST">
          @csrf


          <div class="input-group mb-3">
              <input type= "text" name= "name" class="form-control" placeholder="Full name">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
          </div>
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
            <div class="input-group mb-3">
                <input type="email" class="form-control" name="email" placeholder="Email">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
            </div>
                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
            <div class="input-group mb-3">
                <input type="number" class="form-control" name="umur" placeholder="umur">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
            </div>
                  @error('umur')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror 
            <div class="input-group mb-3">
                <textarea name="alamat" id="" class="form-control" placeholder="Alamat"></textarea>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
            </div>
                @error('alamat')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
            <div class="input-group mb-3">
                <input type="password" class="form-control" name= "password" placeholder="Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
            </div>
                @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
            <div class="input-group mb-3">
              <input type="password" class="form_control" name="password_confirmation" placeholder= "Retype password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div> 
                </div>    
            </div>
            <div class="row">
              <div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                    Remember Me
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">
                  {{ __('Register') }}</button>
              </div>
            </div>
                <!-- /.col -->
            
        </form>

          <a href="/login" class="text-center">Sudah punya akun</a>
        

     @endsection
