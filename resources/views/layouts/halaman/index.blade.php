@extends('layouts.master')

@section('judul')
    Halaman Tambah Data
@endsection

@section('content')
<a href="/post/create" class="btn btn-primary mb-3">Tambah</a>
<div class="row">
    @foreach ($post as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
                
            <img src="{{asset('gambar/ '.$item->gambar)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <p class="card-text"></p>
            </div>
        </div>
    </div>
    @endforeach
</div>
    

@endsection