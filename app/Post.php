<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";
    protected $fillable = ["judul","deskripsi","tag","gambar","galery_id"];
    public $timestamps = false;
}
